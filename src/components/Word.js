import React from "react";

const Word = ({ selectedWord, correctLetters }) => {
    return (
        <div className="word">
            {selectedWord.split("").map((letter, ind) => {
                return (
                    <span className="letter" key={ind}>
                        {correctLetters.includes(letter) ? letter : ""}
                    </span>
                );
            })}
        </div>
    );
};

export default Word;
